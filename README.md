# Commerce Shipping Pickup Magyar Posta

Provides pickup shipping service for Magyar Posta (Hungarian Post), implementing the
[commerce_shipping_pickup_api](https://www.drupal.org/project/commerce_shipping_pickup_api) framework module.

## Providers and services

- [Magyar Posta (Hungarian Post) - PostaPont (Pickup points)](https://www.posta.hu/vallalkozasoknak/postapont)
- [Magyar Posta (Hungarian Post) - PostaPont térképes (Pickup points map)](https://www.posta.hu/vallalkozasoknak/postapont)
- [Magyar Posta (Hungarian Post) - Csomagautomaták (Parcel machines)](https://www.posta.hu/vallalkozasoknak/postapont)

A _Google Maps JavaScript API_ key is required for the map based version to function.
See https://developers.google.com/maps/documentation/javascript/get-api-key.

## Installation

Install and enable the module through Composer:
```
composer require drupal/commerce_shipping_pickup_hupost
drush en commerce_shipping_pickup_hupost
```

Add the new `pickup_capable_shipping_information` pane with the label _Shipping information_
(look for the one that says _Supports pickup: Yes_ in the summary) to your checkout flow
or use the pre-built `pickup` checkout flow.

Add and set up a new shipping method in your store configuration.