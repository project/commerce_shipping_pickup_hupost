(($, Drupal, drupalSettings) => {
  /**
   * Handles the Magyar Posta PostaPont selector in checkout.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.huPostaPontSelected = {
    attach(context) {
      ppapi.linkZipField('pickup_hu_postapont_map_postal_code');
      ppapi.insertMap('pickup-hu-postapont-map-canvas');
      ppapi.onSelect = function (data) {
        $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][id]']").val(data['id']);
        $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][name]']").val(data['name']);
        $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][address]']").val(data['zip'] + " " + data['county'] + " " + data['address']);
      };
    }
  };
})(jQuery, Drupal, drupalSettings);