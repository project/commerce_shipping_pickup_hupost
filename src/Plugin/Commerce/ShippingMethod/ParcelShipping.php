<?php

namespace Drupal\commerce_shipping_pickup_hupost\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping_pickup_api\Plugin\Commerce\ShippingMethod\PickupShippingMethodBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides a specific pickup shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "pickup_hu_postacsomag",
 *   label = @Translation("Pickup shipping - Magyar Posta parcel machines"),
 * )
 */
class ParcelShipping extends PickupShippingMethodBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'cron_interval' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['cron_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Pickup list refresh frequency'),
      '#options' => [
        0 => $this->t('Disabled'),
        3600 => $this->t('Hourly'),
        86400 => $this->t('Daily'),
        604800 => $this->t('Weekly'),
      ],
      '#size' => 1,
      '#default_value' => $this->configuration['cron_interval'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $cron_interval = $this->configuration['cron_interval'] = $values['cron_interval'];

      if ($cron_interval > 0) {
        \Drupal::state()->set('commerce_shipping_pickup_hupost.next_run', \Drupal::time()->getRequestTime() + $cron_interval);
      } else {
        \Drupal::state()->set('commerce_shipping_pickup_hupost.next_run', 0);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(ProfileInterface $profile): array {
    return [
      '#type' => 'select',
      '#title' => $this->t('Select a parcel machine:'),
      '#default_value' => $profile->getData('pickup_location_data'),
      '#options' => $this->loadPickups(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function populateProfile(ProfileInterface $profile): void {
    $id = $profile->getData('pickup_location_data');
    $pickups = $this->loadPickups();
    if (array_key_exists($id, $pickups)) {
      $profile->set('address', [
        'country_code' => 'HU',
        'address_line1' => $pickups[$id],
      ]);
    }
  }

  /**
   * Load pickup points, update from provider if necessary.
   *
   * @return array
   *   Sorted list of pickup points.
   */
  private function loadPickups(): array {
    $pickups = $this->getPickups();
    if (empty($pickups)) {
      commerce_shipping_pickup_hupost_parcel_update();
      $pickups = $this->getPickups();
    }
    return $pickups;
  }

  /**
   * Load pickup points from database.
   *
   * @return array
   *   Sorted list of pickup points.
   */
  private function getPickups(): array {
    $result = \Drupal::database()
      ->select('commerce_shipping_pickup_hupost', 'h')
      ->fields('h', ['serialized'])
      ->condition('h.id', 'pickup_hu_postacsomag')
      ->range(0, 1)
      ->execute();
    foreach ($result as $record) {
      return unserialize($record->serialized);
    }

    return [];
  }
}
