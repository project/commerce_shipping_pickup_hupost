<?php

namespace Drupal\commerce_shipping_pickup_hupost\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping_pickup_api\Plugin\Commerce\ShippingMethod\PickupShippingMethodBase;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a specific pickup shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "pickup_hu_postapont_map",
 *   label = @Translation("Pickup shipping - Magyar Posta PostaPont (map)"),
 * )
 */
class PointMapShipping extends PickupShippingMethodBase {
  /**
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new pickup shipping object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);
    $this->configFactory = $configFactory;

    $this->services['pickup'] = new ShippingService('pickup', $this->configuration['rate_label']);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->configFactory->get('commerce_shipping_pickup_hupost.settings');
    $form['map_api_key'] = [
      '#type' => 'textfield',
      '#title' => t('Google Maps JavaScript API key'),
      '#description' => t('Visit the <a href="@google-maps">Google Maps Platform</a> to get your key.', ['@google-maps' => 'https://developers.google.com/maps/documentation/javascript/get-api-key']),
      '#default_value' => $config->get('google_maps_api_key'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $config = $this->configFactory->getEditable('commerce_shipping_pickup_hupost.settings');
      $config->set('google_maps_api_key', $values['map_api_key']);
      $config->save();
      Cache::invalidateTags(['library_info']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(ProfileInterface $profile): array {
    $element = [
      '#type' => 'container',
    ];
    $element['selector'] = [
      '#type' => 'inline_template',
      '#template' => '<div id="pickup-hu-postapont-map-canvas"></div>',
    ];
    $element['id'] = [
      '#type' => 'hidden',
      '#required' => TRUE,
    ];
    $element['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#attributes' => ['readonly' => 'readonly'],
      '#required' => TRUE,
    ];
    $element['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#attributes' => ['readonly' => 'readonly'],
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function populateProfile(ProfileInterface $profile): void {
    $data = $profile->getData('pickup_location_data');
    $profile->set('address', [
      'country_code' => 'HU',
      'organization' => $data['name'],
      'address_line1' => $data['address'],
    ]);
  }
}
